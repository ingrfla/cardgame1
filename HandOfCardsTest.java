package cardGameTest;

import code.HandOfCards;
import code.PlayingCard;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class HandOfCardsTest {
    /**
     * tests if the flush test is correct with assserttrue
     */

    @Test
    void flushTest(){
        ArrayList<PlayingCard> flush = new ArrayList<>();
        PlayingCard card = new PlayingCard('S',9);
        PlayingCard card1 = new PlayingCard('S',10);
        PlayingCard card2 = new PlayingCard('S',11);
        PlayingCard card3 = new PlayingCard('S',12);
        PlayingCard card4 = new PlayingCard('S',13);
        flush.add(card);
        flush.add(card1);
        flush.add(card2);
        flush.add(card3);
        flush.add(card4);
        HandOfCards hand = new HandOfCards(flush);
        assertTrue(hand.flush());
    }

    /**
     * negative test of the flush method to assert it is false when the hans does not have flush
     */
    @Test
     void negativeFlushTest(){
        ArrayList<PlayingCard> nflush = new ArrayList<>();
    PlayingCard card = new PlayingCard('D',9);
    PlayingCard card1 = new PlayingCard('C',10);
    PlayingCard card2 = new PlayingCard('S',11);
    PlayingCard card3 = new PlayingCard('S',12);
    PlayingCard card4 = new PlayingCard('H',13);
        nflush.add(card);
        nflush.add(card1);
        nflush.add(card2);
        nflush.add(card3);
        nflush.add(card4);
    HandOfCards hand = new HandOfCards(nflush);
    assertFalse(hand.flush());
}

    /**
     * Test to check if the sum test is correct with asserEquals
     */
    @Test
    void sumTest(){
        ArrayList<PlayingCard> sum = new ArrayList<>();
        PlayingCard card = new PlayingCard('S',9);
        PlayingCard card1 = new PlayingCard('S',10);
        PlayingCard card2 = new PlayingCard('S',11);
        PlayingCard card3 = new PlayingCard('S',12);
        PlayingCard card4 = new PlayingCard('S',13);
        sum.add(card);
        sum.add(card1);
        sum.add(card2);
        sum.add(card3);
        sum.add(card4);
        HandOfCards hand = new HandOfCards(sum);
        assertEquals(55,hand.sumOfFaces());
    }

    /**
     * Negative test to double check if sumOfFaces work with assertNotEquals
     */

    @Test
    void sumTest2(){
        ArrayList<PlayingCard> sum = new ArrayList<>();
        PlayingCard card = new PlayingCard('S',9);
        PlayingCard card1 = new PlayingCard('S',10);
        PlayingCard card2 = new PlayingCard('S',11);
        PlayingCard card3 = new PlayingCard('S',12);
        PlayingCard card4 = new PlayingCard('S',13);
        sum.add(card);
        sum.add(card1);
        sum.add(card2);
        sum.add(card3);
        sum.add(card4);
        HandOfCards hand = new HandOfCards(sum);
        assertNotEquals(56,hand.sumOfFaces());
    }


    /**
     * Test to check if the method has queen of spades with assertTrue
     */
    @Test
    void queenOfSpadesTest(){
        ArrayList<PlayingCard> queenOfSpades = new ArrayList<>();
        PlayingCard card = new PlayingCard('H',9);
        PlayingCard card1 = new PlayingCard('H',10);
        PlayingCard card2 = new PlayingCard('S',11);
        PlayingCard card3 = new PlayingCard('S',12);
        PlayingCard card4 = new PlayingCard('S',13);
        queenOfSpades.add(card);
        queenOfSpades.add(card1);
        queenOfSpades.add(card2);
        queenOfSpades.add(card3);
        queenOfSpades.add(card4);
        HandOfCards hand = new HandOfCards(queenOfSpades);
        assertTrue(hand.hasQueenOfSpades());
    }

    /**
     * Negative test to assure queen of spades method works with assertFalse
     */
    @Test
    void queenOfSpadesTest1() {
        ArrayList<PlayingCard> queenOfSpades = new ArrayList<>();
        PlayingCard card = new PlayingCard('H', 9);
        PlayingCard card1 = new PlayingCard('H', 10);
        PlayingCard card2 = new PlayingCard('S', 11);
        PlayingCard card3 = new PlayingCard('S', 18);
        PlayingCard card4 = new PlayingCard('S', 13);
        queenOfSpades.add(card);
        queenOfSpades.add(card1);
        queenOfSpades.add(card2);
        queenOfSpades.add(card3);
        queenOfSpades.add(card4);
        HandOfCards hand = new HandOfCards(queenOfSpades);
        assertFalse(hand.hasQueenOfSpades());
    }

    /**
     * Test to check if cardsOfHearts method works with assertEquals
     */

    @Test
    void cardsOfHeartTest(){
        ArrayList<PlayingCard> heart = new ArrayList<>();
        PlayingCard card = new PlayingCard('H',9);
        PlayingCard card1 = new PlayingCard('S',10);
        PlayingCard card2 = new PlayingCard('S',11);
        PlayingCard card3 = new PlayingCard('H',12);
        PlayingCard card4 = new PlayingCard('S',13);
        heart.add(card);
        heart.add(card1);
        heart.add(card2);
        heart.add(card3);
        heart.add(card4);
        HandOfCards hand = new HandOfCards(heart);
        assertEquals(2,hand.cardsOfHeart().size());

    }

    /**
     * negative test to check if cardOfHearts method works with assertNotEquals
     */
    @Test
    void negativeCardsOfHeartTest(){
        ArrayList<PlayingCard> heart = new ArrayList<>();
        PlayingCard card = new PlayingCard('C',9);
        PlayingCard card1 = new PlayingCard('S',10);
        PlayingCard card2 = new PlayingCard('S',11);
        PlayingCard card3 = new PlayingCard('C',12);
        PlayingCard card4 = new PlayingCard('S',13);
        heart.add(card);
        heart.add(card1);
        heart.add(card2);
        heart.add(card3);
        heart.add(card4);
        HandOfCards hand = new HandOfCards(heart);
        assertNotEquals(2,hand.cardsOfHeart().size());

    }




}
