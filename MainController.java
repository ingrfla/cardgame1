package idatt2001carddgame.carddgame;

import code.DeckOfCards;
import code.HandOfCards;
import code.PlayingCard;
import javafx.fxml.FXML;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

import java.util.List;
import java.util.stream.Collectors;

public class MainController {
   private DeckOfCards deck = new DeckOfCards();
   List<PlayingCard> cardsOnHand;
   /**
    * The suit and face of the card
    */
   @FXML
   public Text sf1, sf2, sf3, sf4, sf5;

   /**
    * the sum of the faces
    */
   @FXML
   public Text sumFaces;

   /**
    * The hearts on hand
    */
   @FXML
   private Text hearts;

   /**
    * will be checked if the hand has flush
    */
   @FXML
   private CheckBox flush;


   /**
    * will be checked if it has queen of spades
    */
   @FXML
   private CheckBox queenOfSpades;


   /**
    * Method that gives value to the cards in the fxml
    */
   @FXML
   public void dealHand() {
flush.setSelected(false);
queenOfSpades.setSelected(false);

      cardsOnHand = deck.dealHand(5);
      sf1.setText(cardsOnHand.get(0).getAsString());
      sf2.setText(cardsOnHand.get(1).getAsString());
      sf3.setText(cardsOnHand.get(2).getAsString());
      sf4.setText(cardsOnHand.get(3).getAsString());
      sf5.setText(cardsOnHand.get(4).getAsString());

   }

   /**
    * Give values to the fxml
    * set heart list to string on the format H1 H2
    * set sum of faces to int
    * set checkbox to true if the hand has flush
    * set the checkbox to true if the hand has queen of spades
    */
   @FXML
   public void checkHand() {

      HandOfCards hand = new HandOfCards(cardsOnHand);

      hearts.setText(hand.cardsOfHeart().stream()
              .map(card -> card.getAsString())
              .collect(Collectors.joining()));
      if(hand.cardsOfHeart().size()==0){
         hearts.setText("No hearts found");
      }
      sumFaces.setText(String.valueOf(hand.sumOfFaces()));
      if (hand.flush())
      flush.setSelected(true);
      if (hand.hasQueenOfSpades())
         queenOfSpades.setSelected(true);


   }
}