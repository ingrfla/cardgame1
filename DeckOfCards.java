package code;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class DeckOfCards {
    private ArrayList<PlayingCard> deckOfCards;
    private final char[] suit = {'S', 'H', 'D', 'C'};
    private int [] face = {1,2,3,4,5,6,7,8,9,10,1,12,13};

    /**
     * Represents a deck of card with 52 cards
     */
    public DeckOfCards() {
        this.deckOfCards = new ArrayList<>();
        final char[] suit = {'S', 'H', 'D', 'C'};
        final int [] face = {1,2,3,4,5,6,7,8,9,10,1,12,13};
        for(int faces: face){
            for(char suits: suit){
                deckOfCards.add(new PlayingCard(suits,faces));
            }
        }
    }


    /**
     * method to get random card from the deck
     * @return a random card
     */
    public PlayingCard getRandom() {
        Random card = new Random();
        return deckOfCards.get(card.nextInt(deckOfCards.size()));

    }

    /**
     * Method to deal random cards from the deck
     * @param n how many cards that will be dealt
     * @return an arraylist with random dealt cards from the deck
     */
    public List<PlayingCard> dealHand(int n) {
        ArrayList<PlayingCard> dealtCards = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            dealtCards.add(this.getRandom());
            //TODO:make random cards not appear again
        }
        return dealtCards;
    }
}
