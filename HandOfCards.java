package code;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class HandOfCards {

    private List<PlayingCard> handOfCards = new ArrayList<>();

    /**
     * constructor that holds an arraylist with random cards from the deck
     * @param handOfCards
     */

    public HandOfCards(List<PlayingCard> handOfCards) {
        this.handOfCards = handOfCards;
    }

    /**
     * checks if the hand has flush
     * @return true if it has or false if not
     */
    public boolean flush() {
        return this.handOfCards.stream().collect(Collectors.groupingBy((PlayingCard::getSuit), Collectors.counting())).values().stream().anyMatch(count -> count >= 5);
    }

    /**
     * checks if the hand of card have queen of spades
     * @return true if the hand has a queen of spades, and false if not
     */
    public boolean hasQueenOfSpades() {
        return this.handOfCards.stream().anyMatch(c -> c.getSuit() == 'S' && c.getFace() == 12);
    }

    /**
     * adds the sum of the faces together
     * @return the sum of the faces
     */

    public int sumOfFaces() {
        return this.handOfCards.stream().mapToInt(PlayingCard::getFace).sum();
    }

    /**
     * A list with the hearts on hand
     * @return a list with the hearts on the hand
     */
    public List<PlayingCard> cardsOfHeart() {
        return this.handOfCards.stream().filter(c -> c.getSuit() == 'H').toList();
    }

}
